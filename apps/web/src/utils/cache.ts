import { Visualisation } from "../types";

type VisualisationCache = Record<string, Visualisation>;

const visualisationCache: VisualisationCache = {
  "v1": {
    id: "v1",
    name: "Visualisation 1",
    data: {

      "1": {
        id: "1",
        title: "Node 1",
        color: "blue",
        position: [0, 0]
      },

      "2": {
        id: "2",
        title: "Node 2",
        color: "red",
        position: [0, 0]
      },

      "3": {
        id: "3",
        title: "Node 3",
        color: "green",
        position: [0, 0]
      }

    }
  }
};

export function getItemFromCache(visId: string) {
  return visualisationCache[visId];
}