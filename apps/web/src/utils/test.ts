import { expect, vi } from "vitest";
import * as visCache from "./cache";

export function mockVisCache() {
  vi.spyOn(visCache, "getItemFromCache").mockReturnValueOnce({
    id: "v1",
    name: "Test",
    data: {
      "1": {
        id: "1",
        color: "blue",
        position: [0, 0],
        title: "Item 1"
      },
      "2": {
        id: "2",
        color: "red",
        position: [0, 0],
        title: "Item 2"
      }
    }
  });
}

export function expectEvery<T>(items: Map<string, T>, selector: (it: T) => any, expected: boolean) {
  [...items.values()].every(it => expect(selector(it)).toBe(expected));
}