import { describe, expect, it, vi } from "vitest";
import { setupVisualisation } from "./useVisualisation";
import * as visCache from "../../utils/cache";
import { expectEvery, mockVisCache } from "../../utils/test";

describe("#setupVisualisation", () => {
  it("should setup visualisation items", () => {
    mockVisCache();
    const sut = setupVisualisation("v1");

    expect(sut.items.size).toBe(2);
    expectEvery(sut.items, (it) => it.value.isSelected, false);
  });

  describe('#clickItem', () => {
    it("should select item single item on click", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");

      sut.clickItem({ id: "1" });

      const item1 = sut.getItem("1");
      const item2 = sut.getItem("2");

      expect(item1!.value.isSelected).toEqual(true);
      expect(item2!.value.isSelected).toEqual(false);

      sut.clickItem({ id: "2" });

      expect(item1!.value.isSelected).toEqual(false);
      expect(item2!.value.isSelected).toEqual(true);

    });

    it("should deselect item on click when it's selected", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");
      sut.selectItem("1");

      sut.clickItem({ id: "1" });

      expect(sut.getItem("1")!.value.isSelected).toEqual(false);
    });

    it("should select multiple items when clicked with shift", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");
      sut.clickItem({ id: "1" });
      sut.clickItem({ id: "2" }, { shiftKey: true });

      const item1 = sut.getItem("1");
      const item2 = sut.getItem("2");

      expect(item1!.value.isSelected).toEqual(true);
      expect(item2!.value.isSelected).toEqual(true);
    });
  })

  describe('#selectItem', () => {
    it("should select item", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");

      sut.selectItem('1')

      expect(sut.getItem('1')!.value.isSelected).toBe(true);
      expect(sut.getItem('2')!.value.isSelected).toBe(false);
    });
  });

  describe('#unselectItem', () => {
    it("should unselect item", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");

      sut.getItem('1').value.isSelected = true;

      sut.unselectItem('1')

      expect(sut.getItem('1')!.value.isSelected).toBe(false);
    });
  });

  describe('#selectedItems', () => {
    it("should make selectedItems changed when selection changes", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");
      expect(sut.selectedItems.value).toHaveLength(0);

      sut.selectItem('1');

      expect(sut.selectedItems.value).toHaveLength(1);
      expect(sut.selectedItems.value).toContainEqual(sut.getItem('1'));
      expect(sut.selectedItems.value).not.toContainEqual(sut.getItem('2'));

      sut.selectItem('2');

      expect(sut.selectedItems.value).toHaveLength(2);
      expect(sut.selectedItems.value).toContainEqual(sut.getItem('1'));
      expect(sut.selectedItems.value).toContainEqual(sut.getItem('2'));


      sut.unselectItem('1');

      expect(sut.selectedItems.value).toHaveLength(1);
      expect(sut.selectedItems.value).not.toContainEqual(sut.getItem('1'));
      expect(sut.selectedItems.value).toContainEqual(sut.getItem('2'));

      sut.unselectItem('2');

      expect(sut.selectedItems.value).toHaveLength(0);
      expect(sut.selectedItems.value).not.toContainEqual(sut.getItem('1'));
      expect(sut.selectedItems.value).not.toContainEqual(sut.getItem('2'));
    });
  })

  describe("#setItemData", () => {
    it("should set color", () => {
      mockVisCache();
      const sut = setupVisualisation("v1");

      sut.setItemData('1', {color: 'green'});

      expect(sut.getItem('1').value.color).toBe('green');
      expect(sut.getItem('2').value.color).toBe('red');
    });
  });
});