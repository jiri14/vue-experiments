import { computed, inject, provide, Ref, ref } from "vue";
import { getItemFromCache } from "../../utils/cache";
import { VisualisationItem } from "../../types";

export interface ExtendedVisualisationItem extends VisualisationItem {
  isSelected: boolean;
}

export function setupVisualisation(visId: string) {

  const items = new Map<string, Ref<ExtendedVisualisationItem>>();

  Object.values(getItemFromCache(visId).data).forEach(it => {
    const visItem = ref<ExtendedVisualisationItem>({ ...it, isSelected: false });
    items.set(it.id, visItem);
  });

  function selectItem(item: string | Pick<VisualisationItem, "id">) {
    const itemId = typeof item === "string" ? item : item.id;
    const value = items.get(itemId)?.value;
    if (value) {
      value.isSelected = true;
    }
  }

  function unselectItem(item: string | Pick<VisualisationItem, "id">) {
    const itemId = typeof item === "string" ? item : item.id;
    const value = items.get(itemId)?.value;
    if (value) {
      value.isSelected = false;
    }
  }

  function unselectAllItems() {
    items.forEach(it => unselectItem(it.value));
  }

  function clickItem(item: Pick<VisualisationItem, "id">, event?: Pick<MouseEvent, "shiftKey">) {
    if (event?.shiftKey) {
      selectItem(item);
      return;
    }
    const wasSelected = getItem(item.id)?.value.isSelected || false;
    unselectAllItems();
    if (!wasSelected) {
      selectItem(item);
    }
  }

  function getItem(id: string) {
    return items.get(id);
  }

  function setItemData(id: string, data: {color: string}) {
    const item = getItem(id);
    if(item) {
      item.value.color = data.color;
    }
  }

  const selectedItems = computed(() => {
    const selected: Ref<ExtendedVisualisationItem>[] = [];
    items.forEach(it => {
      if(it.value.isSelected) selected.push(it)
    })
    return selected;
  })

  return {
    items,
    selectedItems,
    selectItem,
    unselectItem,
    unselectAllItems,
    setItemData,
    clickItem,
    getItem,
  };
}

export type VisualisationController = ReturnType<typeof setupVisualisation>;

export function provideVisualisation(ctrl: VisualisationController) {
  provide("visualisation", ctrl);
}

export function useVisualisation(): VisualisationController {
  const ctx = inject("visualisation");
  if (!ctx) {
    throw new Error("Visualisation controller not available!");
  }
  return ctx as VisualisationController;
}