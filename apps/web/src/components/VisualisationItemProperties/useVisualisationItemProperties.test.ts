import { describe, expect, it, vi } from "vitest";
import { Dependencies, setupVisualisationItemProperties } from "./useVisualisationItemProperties";
import { computed, ref } from "vue";
import { Point, SelectInputEvent } from "../../types";

describe("#setupVisualisationItemProperties", () => {

  it("should be SINGLE STATE", () => {

    const visualisation: Dependencies['visualisation'] = {
      setItemData: vi.fn(),
      selectedItems: computed(() => [ref({
        id: "3",
        title: "Node 3",
        color: "green",
        position: [0, 0] as Point,
        isSelected: true,
      })]),
    }
    //
    const sut = setupVisualisationItemProperties({visualisation});

    expect(sut.state.value.name === 'SINGLE').toBe(true);
    if(sut.state.value.name === 'SINGLE') { // for type guard -> we could write some helper type to avoid name
      sut.state.value.changeColor({target: { value: 'red'} } as SelectInputEvent)
    }

    expect(visualisation.setItemData).toHaveBeenCalledWith('3', { color: 'red' });
  });


});