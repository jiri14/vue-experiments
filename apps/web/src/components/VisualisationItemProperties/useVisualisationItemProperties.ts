import { computed } from "vue";
import type { VisualisationController } from "../Visualisation/useVisualisation";
import { SelectInputEvent } from "../../types";

export type State = {
  name: "NONE";
} | {
  name: "MULTIPLE";
} | {
  name: "SINGLE";
  color: string;
  changeColor: (evt: SelectInputEvent) => void;
}

export interface Dependencies {
  visualisation: Pick<VisualisationController, "selectedItems" | 'setItemData'>;
}

export function setupVisualisationItemProperties(deps: Dependencies) {
  const state = computed<State>(() => {
    const selectedItems = deps.visualisation.selectedItems.value;
    if (selectedItems.length <= 0) {
      return {
        name: "NONE"
      };
    } else if (selectedItems.length > 1) {
      return {
        name: "MULTIPLE"
      };
    } else {
      return {
        name: "SINGLE",
        color: selectedItems[0].value.color,
        changeColor: (evt: SelectInputEvent) => {
            deps.visualisation.setItemData(
              selectedItems[0].value.id,
              {color: evt.target.value}
            );
        }
      };
    }
  });
  return {
    state
  };
}