export interface VisualisationItem {
  id: string;
  title: string;
  color: string;
  position: Point;
}

export interface Visualisation {
  id: string;
  name: string;
  data: Record<string, VisualisationItem>;
}

export type Point = [number, number];

export interface SelectInputEvent extends Event {
  target: HTMLSelectElement;
}